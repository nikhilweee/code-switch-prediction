# Predicting Code Switched Points

This repository contains the code and documentation necessary for predicting code switching points in mixed language text. This is derived from [guillaumegenthial's work on sequence tagging](https://github.com/guillaumegenthial/sequence_tagging), which implements a NER model using Tensorflow (LSTM + CRF + chars embeddings). Check their related [blog post](https://guillaumegenthial.github.io/sequence-tagging-with-tensorflow.html) for more info.

Please go through the documentation folder for a quick summary.